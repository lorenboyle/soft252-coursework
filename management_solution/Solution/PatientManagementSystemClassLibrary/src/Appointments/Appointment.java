/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Appointments;



import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Loren Boyle
 */
public class Appointment implements Serializable{
    
    private String date;
    private int time;
    private String patient;
    private String doctor;
    private boolean approved = false;
    private String notes;
    
    public Appointment(String doctor, String patient, int time, String date, boolean approved) {
        this.doctor = doctor;
        this.patient = patient;
        this.time = time;
        this.date = date;
        this.approved = false;
    }
    
    public Appointment(String patient, String date, String notes) {
        this.patient = patient;
        this.date = date;
        this.notes = notes;
    }
        

    public Appointment() {
    }
    
    public boolean getAppointmentApproved() {
        return approved;
    }
    
    public void setAppointmentApproved(boolean approved) {
        this.approved = !approved;
    }
    
    public String getDate() {
        return date;
    }
    
    public int getTime() {
        return time;
    }
    
    public String getPatientApp() {
        return patient;
    }
    
    public String getDoctorApp() {
        return doctor;
    }
    
    public String getNotes() {
        return notes;
    }
    
    
    
    
    

    


        
}
    
    
    
    
    

