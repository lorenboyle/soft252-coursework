/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DoctorRatings;

import java.io.Serializable;


/**
 *
 * @author Loren Boyle
 */
public class Ratings implements Serializable  {
    
    private String doctorUsername;
    private int ratingValue;
    private String feedback;
    
    public Ratings(String doctorUsername, int ratingValue, String feedback) {
        this.doctorUsername = doctorUsername;
        this.ratingValue = ratingValue;
        this.feedback = feedback;
    }
    
    public Ratings(String doctorUsername, String feedback) {
        this.doctorUsername = doctorUsername;
        this.feedback = feedback;
    }
    
    public String getRatingDoctor() {
        return doctorUsername;
    }
    
    public int getRatingValue() {
        return ratingValue;
    }
    
    public String getRatingFeedback() {
        return feedback;
    }
    
    
}
