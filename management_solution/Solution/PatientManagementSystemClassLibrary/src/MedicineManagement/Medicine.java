/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MedicineManagement;

import java.io.Serializable;

/**
 *
 * @author Loren Boyle
 */
public class Medicine implements Serializable  {
    
    private String medName;
    private String medDosage;
    private int medQuantity;
    private boolean requestOrder = false;
    private String patientName;
    
    public Medicine(String medName, String medDosage, int medQuantity) {
        this.medName = medName;
        this.medDosage = medDosage;
        this.medQuantity = medQuantity;
    }
    
    public Medicine(String medName, String medDosage, String patientName) {
        this.medName = medName;
        this.medDosage = medDosage;
        this.patientName = patientName;       
    }
    
    public int getQuantity() {
        return medQuantity;
    }
    
    public int setQuantityDecrease(int valueDecreasing) {
        
        medQuantity -= valueDecreasing;
        
        return medQuantity;
    }
    
    public int setQuantityIncrease(int valueIncrease) {
        
        medQuantity += valueIncrease;
        
        return medQuantity;
    }
    
    public String getMedName() {
        return medName;
    }
    
    public String getMedDosage() {
        return medDosage;
    }
    
    public boolean getRequestOrder() {
        return requestOrder;
    }
    
    
    public static class NewMedsBuilder implements Serializable {
        
        
        private final String medName;
        private final String dosage;
        
        private int quantity;
        private boolean requestOrder;
        
        public NewMedsBuilder(String medName, String medDosage) {
            this.medName = medName;
            this.dosage = medDosage;
        }
        
        public NewMedsBuilder setQuantity() {
            quantity = 0;
            return this;
        }
        
        public NewMedsBuilder requestSecOrder() {
            requestOrder = true;
            return this;
        }
        
        public Medicine createMed() {
            return new Medicine(this);
        }
        
    }
    
    private Medicine(NewMedsBuilder newMedBuilder) {
        medName = newMedBuilder.medName;
        
    } 
    
    
    
    
    
}
