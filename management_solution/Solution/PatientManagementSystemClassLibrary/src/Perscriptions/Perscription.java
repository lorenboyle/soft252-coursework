/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Perscriptions;

import java.io.Serializable;

/**
 *
 * @author Loren Boyle
 */
public class Perscription implements Serializable {
    
    private String patientID;
    private String medName;
    private String medDosage;
    private boolean delivered = false;
    private String consultNotes;

    
    public Perscription(String patientName, String medName, String medDosage, boolean delivered) {
        
        this.patientID = patientName;
        this.medName = medName;
        this.medDosage = medDosage;
        this.delivered = delivered;
        
    }
    
    public boolean getDelivered() {
        return delivered;
    }
    
    public void setPerscriptionDelivered(boolean perscDelivered) {
        this.delivered = !perscDelivered;
    }
    
    public String getPerscPatient() {
        return patientID;
    }
    
    public String getPerscMedName() {
        return medName;
    }
    
    public String getPerscMedDosage() {
        return medDosage;
    }
    
    public String getPerscConsultNotes() {
        return consultNotes;
    }
    
}
