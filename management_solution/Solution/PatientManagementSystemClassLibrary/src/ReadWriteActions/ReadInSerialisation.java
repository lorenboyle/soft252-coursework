package ReadWriteActions;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import Appointments.Appointment;
import DoctorRatings.Ratings;

import MedicineManagement.Medicine;
import Perscriptions.Perscription;
import SolutionUsers.Admin;
import SolutionUsers.Doctor;
import SolutionUsers.Patient;
import SolutionUsers.Secretary;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;


/**
 *
 * @author Loren Boyle
 */
public class ReadInSerialisation implements Serializable {
    
    
   public ArrayList<Admin> readingInAdminFiles(String filePath) {
       ArrayList<Admin> adminFile = new ArrayList<>();
       
       try {
           FileInputStream fileIn = new FileInputStream(filePath);
           ObjectInputStream in = new ObjectInputStream(fileIn);
           
           ArrayList<Admin> adminFromFile = (ArrayList) in.readObject();
           adminFile.addAll(adminFromFile);
           
           in.close();
           fileIn.close();
       } catch (IOException e) {
           e.printStackTrace();
       } catch (ClassNotFoundException ex) {
           ex.printStackTrace();
       }
       
       return adminFile;
   }
   
   public ArrayList<Patient> readingInPatientFiles(String filePath) {
       ArrayList<Patient> patientFile = new ArrayList<>();
       
       try {
           FileInputStream fileIn = new FileInputStream(filePath);
           ObjectInputStream in = new ObjectInputStream(fileIn);
           
           ArrayList<Patient> patientsFromFile = (ArrayList) in.readObject();
           patientFile.addAll(patientsFromFile);
           
           
           in.close();
           fileIn.close();
       } catch (IOException e) {
           e.printStackTrace();
       } catch (ClassNotFoundException ex) {
           ex.printStackTrace();
       }
       
       return patientFile;
   }
   
   
   public ArrayList<Doctor> readingInDoctorFiles(String filePath) {
       ArrayList<Doctor> doctorFile = new ArrayList<>();
       
       try {
           FileInputStream fileIn = new FileInputStream(filePath);
           ObjectInputStream in = new ObjectInputStream(fileIn);
           
           ArrayList<Doctor> doctorFromFile = (ArrayList) in.readObject();
           doctorFile.addAll(doctorFromFile);
           
           in.close();
           fileIn.close();
       } catch (IOException e) {
           e.printStackTrace();
       } catch (ClassNotFoundException ex) {
           ex.printStackTrace();
       }
       
       return doctorFile;
   }
   
   public ArrayList<Secretary> readingInSecretaryFiles(String filePath) {
       ArrayList<Secretary> secFile = new ArrayList<>();
       
       try {
           FileInputStream fileIn = new FileInputStream(filePath);
           ObjectInputStream in = new ObjectInputStream(fileIn);
           
           ArrayList<Secretary> secFromFile = (ArrayList) in.readObject();
           secFile.addAll(secFromFile);
           
           in.close();
           fileIn.close();
       } catch (IOException e) {
           e.printStackTrace();
       } catch (ClassNotFoundException ex) {
           ex.printStackTrace();
       }
       
       return secFile;
   }
   
   public ArrayList<Medicine> readingInMedicineStockFiles(String filePath) {
       ArrayList<Medicine> medFile = new ArrayList<>();
       
       try {
           FileInputStream fileIn = new FileInputStream(filePath);
           ObjectInputStream in = new ObjectInputStream(fileIn);
           
           ArrayList<Medicine> medFromFile = (ArrayList) in.readObject();
           medFile.addAll(medFromFile);
           
           in.close();
           fileIn.close();
       } catch (IOException e) {
           e.printStackTrace();
       } catch (ClassNotFoundException ex) {
           ex.printStackTrace();
       }
       
       return medFile;
   }
   
   public ArrayList<Perscription> readingInPerscriptionFile(String filePath) {
       ArrayList<Perscription> perscFile = new ArrayList<>();
       
       try {
           FileInputStream fileIn = new FileInputStream(filePath);
           ObjectInputStream in = new ObjectInputStream(fileIn);
           
           ArrayList<Perscription> perscFromFile = (ArrayList) in.readObject();
           perscFile.addAll(perscFromFile);
           
           in.close();
           fileIn.close();
       } catch (IOException e) {
           e.printStackTrace();
       } catch (ClassNotFoundException ex) {
           ex.printStackTrace();
       }
       
       return perscFile;
   }
   
   
   public ArrayList<Appointment> readingInAppointmentFile(String filePath) {
       ArrayList<Appointment> appFile = new ArrayList<>();
       
       try {
           FileInputStream fileIn = new FileInputStream(filePath);
           ObjectInputStream in = new ObjectInputStream(fileIn);
           
           ArrayList<Appointment> appFromFile = (ArrayList) in.readObject();
           appFile.addAll(appFromFile);
           
           in.close();
           fileIn.close();
       } catch (IOException e) {
           e.printStackTrace();
       } catch (ClassNotFoundException ex) {
           ex.printStackTrace();
       }
       
       return appFile;
   }
   
   public ArrayList<Ratings> readingInRatingsFiles(String filePath) {
       ArrayList<Ratings> rateFile = new ArrayList<>();
       
       try {
           FileInputStream fileIn = new FileInputStream(filePath);
           ObjectInputStream in = new ObjectInputStream(fileIn);
           
           ArrayList<Ratings> rateFromFile = (ArrayList) in.readObject();
           rateFile.addAll(rateFromFile);
           
           in.close();
           fileIn.close();
       } catch (IOException e) {
           e.printStackTrace();
       } catch (ClassNotFoundException ex) {
           ex.printStackTrace();
       }
       
       return rateFile;
   }
}
