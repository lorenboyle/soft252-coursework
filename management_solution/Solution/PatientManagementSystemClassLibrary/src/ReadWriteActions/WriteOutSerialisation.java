/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ReadWriteActions;

import Appointments.Appointment;
import DoctorRatings.Ratings;
import MedicineManagement.Medicine;
import Perscriptions.Perscription;
import SolutionUsers.Admin;
import SolutionUsers.Doctor;
import SolutionUsers.Patient;
import SolutionUsers.Secretary;
import SolutionUsers.User;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Loren Boyle
 */
public class WriteOutSerialisation  implements Serializable {
    
    public void writeInUserFile(String filePath, ArrayList<User> userList) {
        
        try {
            FileOutputStream fileOut = new FileOutputStream(filePath);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            
            out.writeObject(userList);
            
            out.close();
            fileOut.close();
            
            
        } catch(IOException e) {
            e.printStackTrace();
        }
        
    }    
    
    public void writeInAdminFile(String filePath, ArrayList<Admin> adminList) {
        
        try {
            FileOutputStream fileOut = new FileOutputStream(filePath);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            
            out.writeObject(adminList);
            
            out.close();
            fileOut.close();
            
            
        } catch(IOException e) {
            e.printStackTrace();
        }
        
    }
    
    public void writeInPatientFile(String filePath, ArrayList<Patient> patientList) {
        
        try {
            FileOutputStream fileOut = new FileOutputStream(filePath);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            
            out.writeObject(patientList);
            
            out.close();
            fileOut.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
    
    public void writeInDoctorFile(String filePath, ArrayList<Doctor> doctorList) {
        
        try {
            FileOutputStream fileOut = new FileOutputStream(filePath);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            
            out.writeObject(doctorList);
            
            out.close();
            fileOut.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
    
    public void writeInSecretaryFile(String filePath, ArrayList<Secretary> secretaryList) {
        
        try {
            FileOutputStream fileOut = new FileOutputStream(filePath);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            
            out.writeObject(secretaryList);
            
            out.close();
            fileOut.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
    
    public void writeInMedicineStockFile(String filePath, ArrayList<Medicine> medStock) {
        
        try {
            FileOutputStream fileOut = new FileOutputStream(filePath);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            
            out.writeObject(medStock);
            
            out.close();
            fileOut.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
    
    public void writeInRatingsFile(String filePath, ArrayList<Ratings> ratings) {
        try {
            FileOutputStream fileOut = new FileOutputStream(filePath);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            
            out.writeObject(ratings);
            
            out.close();
            fileOut.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
    
    
    
    public void writeInAppointmentsFile(String filePath, ArrayList<Appointment> appointmentList) {
        try {
            FileOutputStream fileOut = new FileOutputStream(filePath);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            
            out.writeObject(appointmentList);
            
            out.close();
            fileOut.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
    } 
    
    
    public void writeInPerscriptionFile(String filePath, ArrayList<Perscription> perscriptionList) {
        try {
            FileOutputStream fileOut = new FileOutputStream(filePath);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            
            out.writeObject(perscriptionList);
            
            out.close();
            fileOut.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }  
}
