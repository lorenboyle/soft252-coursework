/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestNewUser;

/**
 *
 * @author Loren Boyle
 */
public interface IObserverable {
    
    boolean registerObserver(IObserver observer);
    boolean removeObserver(IObserver observer);
    void notifySecretary();
    
}
