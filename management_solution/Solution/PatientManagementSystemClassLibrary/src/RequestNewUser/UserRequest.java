/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RequestNewUser;

import java.util.ArrayList;

/**
 *
 * @author Loren Boyle
 */
public abstract class UserRequest implements IObserverable {
    
    protected String userID;
    protected String name;
    protected String password;
    protected int age;
    protected boolean terminate;
    private ArrayList<IObserver> newPatients = null;
    
    public UserRequest() {
        
    }
    
    public UserRequest(String userID, String name, String password, int age) {
        this.userID = userID;
        this.name = name;
        this.password = name;
        this.age = age;
    }
    
    public UserRequest(boolean terminate) {
        this.terminate = terminate;
    }
    
    public String getName() {
        return name;
    }
    
    
    @Override
    public boolean registerObserver(IObserver observer) {
        boolean patientRequest = false;
        
        if (observer != null) {
            
        
            if(this.newPatients == null) {
                
                this.newPatients = new ArrayList<>();
            }
        }
            
            if(!this.newPatients.contains(observer)) {
                patientRequest = this.newPatients.add(observer);
            }
        return patientRequest;
    }
    
    @Override
    public boolean removeObserver(IObserver observer){
        boolean removeRequest = false;
        
        if(observer != null) {
            if(this.newPatients != null && this.newPatients.size() > 0) {
                removeRequest = this.newPatients.remove(observer);
            }
        }
        
        
        return removeRequest;
    }
    
    @Override
    public void notifySecretary() {
        
        if(this.newPatients != null && this.newPatients.size() > 0) {
            
            for(IObserver currentPatient : this.newPatients) {
                currentPatient.update();
            }
        }
        
    }
    
    
    
}
