/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SolutionUsers;


import DoctorRatings.Ratings;
import ReadWriteActions.ReadInSerialisation;
import ReadWriteActions.WriteOutSerialisation;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Loren Boyle
 */
public class Admin extends User {
    
    ReadInSerialisation readIn = new ReadInSerialisation();
    WriteOutSerialisation writeOut = new WriteOutSerialisation();
    
    List<Admin> adminList = new ArrayList<>();
    List<Secretary> secList = new ArrayList<>();
    List<Doctor> docList = new ArrayList<>();
    
    
    public String adminFilePath = "src\\Files\\LoginUsers\\admin.ser";
    
    
    public Admin(){
    }
    
    public Admin(String userID, String password, int age, char gender, boolean active, boolean terminate) {
        super(userID, password, 0, 'm', true, false);
    }
    



    
    
    
    
    public void CreateAdminUser(String userID, String password) {
        
        
        Admin newAdmin = new Admin(userID, password, 0, 'm', true, false);
        
        ArrayList<Admin> readAdminList = readIn.readingInAdminFiles(adminFilePath);
        
        readAdminList.add(newAdmin);
        
        writeOut.writeInAdminFile(adminFilePath, readAdminList);    
        
        
        
    }
    
    public void CreateSecretaryUser(String username, String password) {
        
        Secretary newSec = new Secretary(username, password, 0, 'm', true, false);
        
        ArrayList<Secretary> allSec = readIn.readingInSecretaryFiles("src/Files/LoginUsers/secretary.ser");
        
        allSec.add(newSec);
        
        writeOut.writeInSecretaryFile("src/Files/LoginUsers/secretary.ser", allSec);
        
    }
    
    public void DeleteSecretaryUser(String username) {
        
        //Reads in secretary users
        ArrayList<Secretary> allSec = readIn.readingInSecretaryFiles("src/Files/LoginUsers/secretary.ser");
        
        //Searches list
        for(Secretary secretary : allSec) {
            //Finds secretary user with the unique username entered
            if(secretary.username.equals(username)) {
                //remove that object (secretary) from the list
                allSec.remove(secretary);
                break;
            }
        }
        //write out new object list
        writeOut.writeInSecretaryFile("src/Files/LoginUsers/secretary.ser", allSec);
    }
    
    public void CreateDoctorUser(String username, String password) {
        
        Doctor newDoc = new Doctor(username, password, 0, 'm', true, false);
        
        ArrayList<Doctor> readInDocList = readIn.readingInDoctorFiles("src/Files/LoginUsers/doctor.ser");
        
        readInDocList.add(newDoc);
        
        writeOut.writeInDoctorFile("src/Files/LoginUsers/doctor.ser", readInDocList);
    }
    
        public void DeleteDoctorUser(String username) {
        
        //Reads in doctor users
        ArrayList<Doctor> allDocs = readIn.readingInDoctorFiles("src/Files/LoginUsers/doctor.ser");
        
        //Searches list
        for(Doctor doctor : allDocs) {
            //Finds secretary user with the unique username entered
            if(doctor.getUsername().equals(username)) {
                //remove that object (doctor) from the list
                allDocs.remove(doctor);
                break;
            }
        }
        //write out new object list
        writeOut.writeInDoctorFile("src/Files/LoginUsers/doctor.ser", allDocs);
    }
        
       public ArrayList<Ratings> viewDoctorRatings() {
           
             //Creating arrayList for doctor ratings
             ArrayList<Ratings> doctorRatings = readIn.readingInRatingsFiles("src/Files/Feedback/patientDoctorRatings.ser"); //Reads in the patients ratings/feedback of doctors
             
             //Returning values to Client
             return doctorRatings;
             
            
        }
         public void createDoctorFeedback(String doctor, String doctorFeedback) {
             
             ArrayList<Ratings> doctorFeedbackList = readIn.readingInRatingsFiles("src/Files/Feedback/compiledFeedback.ser");
             
             Ratings newDoctorFeedback = new Ratings(doctor, doctorFeedback);
             
             doctorFeedbackList.add(newDoctorFeedback);
             
             writeOut.writeInRatingsFile("src/Files/Feedback/compiledFeedback.ser", doctorFeedbackList);
    
         }
    
}
