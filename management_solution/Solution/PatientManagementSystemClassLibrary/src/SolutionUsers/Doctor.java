/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SolutionUsers;

import Appointments.Appointment;
import DoctorRatings.Ratings;

import ReadWriteActions.ReadInSerialisation;
import ReadWriteActions.WriteOutSerialisation;
import java.util.ArrayList;

import MedicineManagement.Medicine;
import MedicineManagement.Medicine.NewMedsBuilder;
import Perscriptions.Perscription;


/**
 *
 * @author Loren Boyle
 */
public class Doctor extends User {

    ReadInSerialisation readIn = new ReadInSerialisation();
    WriteOutSerialisation writeOut = new WriteOutSerialisation();
    
    ArrayList<Ratings> doctorFeedback = readIn.readingInRatingsFiles("src/Files/Feedback/compiledFeedback.ser");
    
    ArrayList<Doctor> doctorList = readIn.readingInDoctorFiles("src/Files/LoginUsers/doctor.ser");
   
    
    public Doctor() {

    }
    
    public Doctor(String userID, String password, int age, char gender, boolean active, boolean terminate) {
        super(userID, password, 0, 'm', true, false);
    }
    
    

    
    public ArrayList<Appointment> viewDoctorAppointments(String doctorName) {
        
        ArrayList<Appointment> appointmentSearched = null;
        
        ArrayList<Appointment> allAppointments = readIn.readingInAppointmentFile("src/Files/Appointments/appointments.ser");
        
        for(Appointment appointment : allAppointments) {
            if(appointment.getDoctorApp().equals(doctorName)) {
                appointmentSearched.add(appointment);
            }
            
        }
        
        return appointmentSearched;
    }
    
    public void createConsultationNote(String patientUsername, String notes, String date) {
        
        
        ArrayList<Appointment> oldConsultNotes = readIn.readingInAppointmentFile("src/Files/Appointments/consultNotes.ser");
        
        Appointment newNote = new Appointment(patientUsername, date, notes);
        
        oldConsultNotes.add(newNote);
        
        writeOut.writeInAppointmentsFile("src/Files/Appointments/consultNotes.ser", oldConsultNotes);
        
        
        
    }
    
    
    
    
    public void perscribePatient(String patientUsername, String medName, String medDosage) {
        
        
        
        ArrayList<Medicine> allMedInStock = readIn.readingInMedicineStockFiles("src/Files/MedicineStock/medsInStock.ser");
        
        ArrayList<Perscription> allPerscs = readIn.readingInPerscriptionFile("src/Files/MedicineStock/perscriptions.ser");
        
        // For every medicine in stock...
        for(Medicine med : allMedInStock) {
            // if med is in stock...
            if(med.getMedName().equals(medName) && med.getMedDosage().equals(medDosage) && med.getQuantity() != 0) {
                //Create new perscription
                for(Perscription persc : allPerscs) {
                    //if new perscription already exists and hasnt been delivered...
                    if(persc.getPerscMedName().equals(medName) && persc.getDelivered() == false) {
                        break;
                    }
                    else {
                        // Create new perscription
                        Perscription newPersc = new Perscription(patientUsername, medName, medDosage, false);
                        
                        // add perscription to list and write back
                        allPerscs.add(newPersc);
                    
                   
                    
                        writeOut.writeInPerscriptionFile("src/Files/MedicineStock/perscriptions.ser", allPerscs);
                        break;
                    }
                    
                }
                break;
            }
        }
        
        
        
        
    }
    
    public void requestFutureAppointment(String patientUsername, String doctorUsername, String appDate, String appTime) {
        boolean appExists = false;
        String[] splitTime = appTime.split(":");
        int time = Integer.parseInt(splitTime[0]);
        
        
        Appointment requestedAppointment = new Appointment(doctorUsername, patientUsername, time, appDate, false);
        
        ArrayList<Appointment> currentAppointments = readIn.readingInAppointmentFile("src/Files/Appointments/appointments.ser");
        
        for(Appointment apps : currentAppointments) {
            if(apps.getTime() == time && apps.getDate().equals(appDate)) {
                appExists = true;
                break;
            }

        }
        if(appExists) {
        } else {
            currentAppointments.add(requestedAppointment);
        }
        
        writeOut.writeInAppointmentsFile("src/Files/Appointments/appointments.ser", currentAppointments);
        
        
        
    }
    
    
    public void createNewMed(String medName, String dosage, boolean requestedOrder) { 
      
        ArrayList<Medicine> allMedsInStock = readIn.readingInMedicineStockFiles("src/Files/MedicineStock/medsInStock.ser");
        
        if(requestedOrder) {
            Medicine newMed = new NewMedsBuilder(medName, dosage).requestSecOrder().setQuantity().createMed();  
            allMedsInStock.add(newMed);
        }
        else {
            Medicine newMed = new NewMedsBuilder(medName, dosage).setQuantity().createMed();
            allMedsInStock.add(newMed);
        }
        
        writeOut.writeInMedicineStockFile("src/Files/MedicineStock/medsInStock.ser", allMedsInStock);
        
        
        
        
    }
    
    public ArrayList<Appointment> viewAppointments(String doctorName) {
        ArrayList<Appointment> docAppList = new ArrayList<>();
        ArrayList<Appointment> allApps = readIn.readingInAppointmentFile("src/Files/Appointments/appointments.ser");
        
        for(Appointment apps : allApps) {
            if(apps.getDoctorApp().equals(doctorName)) {
                docAppList.add(apps);
            }
        }
        
        return docAppList;
    }
    
    public ArrayList<Appointment> viewAppNotes(String patientName) {
        ArrayList<Appointment> docAppList = new ArrayList<>();
        ArrayList<Appointment> allApps = readIn.readingInAppointmentFile("src/Files/Appointments/consultNotes.ser");
        
        for(Appointment apps : allApps) {
            if(apps.getDoctorApp().equals(patientName) && apps.getAppointmentApproved() == true) {
                docAppList.add(apps);
            }
        }
        
        return docAppList;
    }
    
    
    
    
    
    
    
}
