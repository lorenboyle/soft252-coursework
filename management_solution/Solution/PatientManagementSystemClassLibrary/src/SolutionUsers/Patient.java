/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SolutionUsers;

import Appointments.Appointment;
import DoctorRatings.Ratings;
import Perscriptions.Perscription;
import ReadWriteActions.ReadInSerialisation;
import ReadWriteActions.WriteOutSerialisation;
import java.util.ArrayList;


/**
 *
 * @author Loren Boyle
 */
public class Patient extends User {
    
    ReadInSerialisation readIn = new ReadInSerialisation();
    WriteOutSerialisation writeOut = new WriteOutSerialisation();
    Appointment checkTime = new Appointment();
    Appointment checkDate = new Appointment();
    
    
    public String patientFilePath = "src/Files/LoginUsers/patient.ser";
    
    public Patient(){
    }
    
    public Patient(String userID, String password, int age, char gender, boolean active, boolean terminate){
        super(userID, password, age, gender, active, terminate);
    }
    
    

   
    

    
    public void rateDoctor(String doctorName, int ratingValue, String patientFeedback) {
        
        ArrayList<Ratings> currentRatings = readIn.readingInRatingsFiles("src/Files/Feedback/patientDoctorRatings.ser");
        
        Ratings newRating = new Ratings(doctorName, ratingValue, patientFeedback);
        
        currentRatings.add(newRating);
        
        writeOut.writeInRatingsFile("src/Files/Feedback/patientDoctorRatings.ser", currentRatings); 
     
    }
    
    public double viewDoctorRating(String doctorName) {
        double averageRating = 0;
        
        ArrayList<Ratings> allRatings = readIn.readingInRatingsFiles("src/Files/Feedback/patientDoctorRatings.ser");
        
        ArrayList<Ratings> doctorRatings = new ArrayList<>();
        
        for(Ratings rating : allRatings) {
            if(rating.getRatingDoctor().equals(doctorName)) {
                doctorRatings.add(rating);
            }
        }
        
        for(Ratings rating : doctorRatings) {
            averageRating = averageRating + rating.getRatingValue();
        }
        
        if(averageRating != 0) {
            averageRating = averageRating / doctorRatings.size();
        }
        else {
            averageRating = 0;
        }
        
        return averageRating;
        
        
    } 
    
    public void requestCreatePatientAccount(String username, String password, int age, char gender, boolean approved, boolean terminate) {
        
        
        
        ArrayList<Patient> patientList = readIn.readingInPatientFiles(patientFilePath);
        
        Patient newPatient = new Patient(username, password, age, gender, approved, terminate);
        
        patientList.add(newPatient);
        
        writeOut.writeInPatientFile(patientFilePath, patientList);
        
    }
    
    public void requestTerminateAccount(String currentUser) {
        
        //Readming in patient users to list
        ArrayList<Patient> patientList = readIn.readingInPatientFiles(patientFilePath);
        
        //Setting the patient requesting termination to true
        for(Patient patient : patientList) {
            if(patient.getUsername().equals(currentUser)) {
                patient.setTerminate(patient.getTerminate());
            }
        }
        
        writeOut.writeInPatientFile(patientFilePath, patientList);
    }
    
    public ArrayList<Ratings> ViewDoctorRatings(String doctorUsername) {
        
        ArrayList<Ratings> doctorRatings = readIn.readingInRatingsFiles("src/Files/Feedback/patientDoctorRatings");
        ArrayList<Ratings> doctorSearch = new ArrayList<Ratings>();
        
        for(Ratings rating : doctorRatings) {
            if(rating.getRatingDoctor().equals(doctorUsername)) {
                doctorSearch.add(rating);
            }
            
        }
        
        return doctorSearch;
        
    }
    
    
    public void requestAppointment(String doctorUsername, String patientUsername, String date, String time) {
        boolean exists = false;
        String[] splitTime = time.split(":");
        int appTime = Integer.parseInt(splitTime[0]);
        
        
        Appointment requestedAppointment = new Appointment(doctorUsername, patientUsername, appTime, date, false);
        
        ArrayList<Appointment> currentAppointments = readIn.readingInAppointmentFile("src/Files/Appointments/appointments.ser");
        
        for(Appointment apps : currentAppointments) {
            if(apps.getTime() == appTime && apps.getDate().equals(date)) {
                exists = true;
                break;
            }
        }
        
        if(exists) {
            
        } else {
            currentAppointments.add(requestedAppointment);
        }
        
        writeOut.writeInAppointmentsFile("src/Files/Appointments/appointments.ser", currentAppointments);
          
  

    }
    
    public ArrayList<Appointment> viewAppointments(String username) {
       ArrayList<Appointment> returnApp = new ArrayList<>();
       
       ArrayList<Appointment> allApps = readIn.readingInAppointmentFile("src/Files/Appointments/appointments.ser");
       
       for(Appointment app : allApps){
           if(app.getPatientApp().equals(username)) {
               returnApp.add(app);
           }
       }
       
       return returnApp;  
    }
    
    public ArrayList<Perscription> viewPerscription(String username) {
       ArrayList<Perscription> returnPersc = new ArrayList<>();
       
       ArrayList<Perscription> allPerscs = readIn.readingInPerscriptionFile("src/Files/MedicineStock/perscriptions.ser");
       
       for(Perscription persc : allPerscs){
           if(persc.getPerscPatient().equals(username)) {
               returnPersc.add(persc);
           }
       }
       
       return returnPersc;  
    }
    
}
