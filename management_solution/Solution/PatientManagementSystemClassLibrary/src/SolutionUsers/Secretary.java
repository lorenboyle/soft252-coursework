/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SolutionUsers;

import Appointments.Appointment;
import MedicineManagement.Medicine;
import Perscriptions.Perscription;
import ReadWriteActions.ReadInSerialisation;
import ReadWriteActions.WriteOutSerialisation;
import java.util.ArrayList;

/**
 *
 * @author Loren Boyle
 */
public class Secretary extends User {
    
    ReadInSerialisation readIn = new ReadInSerialisation();
    WriteOutSerialisation writeOut = new WriteOutSerialisation();

    
    
    public Secretary() {
    }

    public Secretary(String userID, String password, int age, char gender, boolean active, boolean terminate) {
        super(userID, password, 0, 'm', true, false);
    }
    
    


    
    public void approvePatientAccount(String userID) {
        
        ArrayList<Patient> patientUsers = readIn.readingInPatientFiles("src/Files/LoginUsers/patient.ser");
        
        for(Patient patient : patientUsers) {
            if(patient.getUsername().equals(userID) && patient.getActive() == false) {
                
                Patient approvedPatient = new Patient(patient.getUsername(), patient.getPassword(), patient.getAge(), patient.getGender(), true, false);
                
                patientUsers.remove(patient);
                patientUsers.add(approvedPatient);
                break;
            }
        }
        
        writeOut.writeInPatientFile("src/Files/LoginUsers/patient.ser", patientUsers);

        
    }
    
    public void approveAppointments(String patientName, String doctorName, String appDate, String appTime) {
        
        String[] splitTime = appTime.split(":");
        int time = Integer.parseInt(splitTime[0]);
        
        
        ArrayList<Appointment> allApps = readIn.readingInAppointmentFile("src/Files/Appointments/appointments.ser");
        
        for(Appointment app : allApps) {
            if(app.getPatientApp().equals(patientName) && app.getDoctorApp().equals(doctorName) && app.getDate().equals(appDate) && app.getTime() == time) {
                app.setAppointmentApproved(app.getAppointmentApproved());
            }
        }
        
        writeOut.writeInAppointmentsFile("src/Files/Appointments/appointments.ser", allApps);
    }
    
    public void givePatientMed(String patientName, String medName, String medDosage) {
        
       
        
        ArrayList<Medicine> patientMeds = readIn.readingInMedicineStockFiles("src/Files/MedicineStock/patientMedTracker.ser");
        
        ArrayList<Medicine> medicineList = readIn.readingInMedicineStockFiles("src/Files/MedicineStock/medsInStock.ser");
        
        ArrayList<Perscription> perscList = readIn.readingInPerscriptionFile("src/Files/MedicineStock/perscriptions.ser");
        
        
        
        
        
        for(Medicine med : medicineList) {
            if(med.getMedName().equals(medName) && med.getMedDosage().equals(medDosage) && med.getQuantity() != 0) {
                for(Perscription persc : perscList) {
                    if(persc.getPerscPatient().equals(patientName) && persc.getPerscMedName().equals(medName) && persc.getPerscMedDosage().equals(medDosage)) {
                        //Change perscription to delivered
                        persc.setPerscriptionDelivered(persc.getDelivered());
                        //create new instance
                        Medicine patientMed = new Medicine(medName, medDosage, patientName);
                        //add to list of patient medicated
                        patientMeds.add(patientMed);
                        //decrease the quanitty avaliable
                        med.setQuantityDecrease(1);
          
                        //write out 
                        writeOut.writeInPerscriptionFile("src/Files/MedicineStock/perscriptions.ser", perscList);
                        writeOut.writeInMedicineStockFile("src/Files/MedicineStock/medsInStock.ser", medicineList);       
                        
                        break;
                    } else {
                        break;
                    }
                    
                }
                break;

            }
            else {
                break;
            }
        }
        
        writeOut.writeInMedicineStockFile("src/Files/MedicineStock/patientMedTracker.ser", patientMeds);
        
        
        
    }
    
    public void orderMeds(String medName, String medDosage, int qty) {
        
        ArrayList<Medicine> medList = readIn.readingInMedicineStockFiles("src/Files/MedicineStock/medsInStock.ser");
        
        for(Medicine med : medList) {
            if(med.getMedDosage().equals(medDosage) && med.getMedName().equals(medName)) {
                med.setQuantityIncrease(qty);
            }
        }
        
        writeOut.writeInMedicineStockFile("src/Files/MedicineStock/medsInStock.ser", medList);
        
        
        
    }
    
    public void approvePatientRemoval(String patientUsername) {
        
        ArrayList<Patient> patientList = readIn.readingInPatientFiles("src/Files/LoginUsers/patient.ser");
        
        for(Patient patient : patientList) {
            if(patient.getUsername().equals(patientUsername)){
                patient.setTerminate(patient.getTerminate());
        
                break;
            }
        }
        
        writeOut.writeInPatientFile("src/Files/LoginUsers/patient.ser", patientList);
    }
    
    public void removePatient(String patientUsername) {
        
        ArrayList<Patient> allPatients = readIn.readingInPatientFiles("src/Files/LoginUsers/patient.ser");
        
        for(Patient patient : allPatients) {
            if(patient.getUsername().equals(patientUsername)){
                allPatients.remove(patient);
                break;
            }
        }
        
        writeOut.writeInPatientFile("src/Files/LoginUsers/patient.ser", allPatients);
        
    }
    
    public ArrayList<Patient> viewPendingPatients() {
        ArrayList<Patient> pendingPatients = new ArrayList<>();
        ArrayList<Patient> allPatients = new ArrayList<>();
        
        allPatients = readIn.readingInPatientFiles("src/Files/LoginUsers/patient.ser");
        
        for(Patient patient : allPatients) {
            if(patient.getActive() == false) {
                pendingPatients.add(patient);
            }
        }
        
        return pendingPatients;
    }
    
    public ArrayList<Appointment> viewPendingAppointments() {
        
        ArrayList<Appointment> pendingPatientApps = new ArrayList<>();
        
        
        ArrayList<Appointment> allApps = readIn.readingInAppointmentFile("src/Files/Appointments/appointments.ser");
        
        for(Appointment app : allApps) {
            if(app.getAppointmentApproved() == false) {
                pendingPatientApps.add(app);
            }
        }
        
        return pendingPatientApps;
    }
    
    public ArrayList<Medicine> viewMedStock(){

        
        ArrayList<Medicine> allMedsInStock = readIn.readingInMedicineStockFiles("src/Files/MedicineStock/medsInStock.ser");
      
        return allMedsInStock;
    }
    
    public ArrayList<Medicine> viewPendingMedOrders() {
        ArrayList<Medicine> orderMedList = new ArrayList<>();

        
        ArrayList<Medicine> allMedsInStock = readIn.readingInMedicineStockFiles("src/Files/MedicineStock/medsInStock.ser");
        
        //for every medicine in the list
        for(Medicine meds : allMedsInStock) {
            //if it has been selected as to be ordered
            if(meds.getRequestOrder() == true) {
                //add to list of view
                orderMedList.add(meds);
            }
        }
      
        return orderMedList;
    }
    
    public void removeRequest(String username) {
        
        
        ArrayList<Patient> patientList = readIn.readingInPatientFiles("src/Files/LoginUsers/patient.ser");
        
        for(Patient patient : patientList) {
            if(patient.getUsername().equals(username) && patient.getActive() == false) {
                patientList.remove(patient);
                break;
            }
        }
        
        writeOut.writeInPatientFile("src/Files/LoginUsers/patient.ser", patientList);
        
        
    }
    
    public ArrayList<Patient> viewActivePatients() {
        
        
        ArrayList<Patient> activePatients = new ArrayList<>();
        
        ArrayList<Patient> allPatients = readIn.readingInPatientFiles("src/Files/LoginUsers/patient.ser");
        
        for(Patient patient : allPatients) {
            if(patient.getActive() == true) {
                activePatients.add(patient);
            }
        }
        
        return activePatients;
    }
    
    
}
