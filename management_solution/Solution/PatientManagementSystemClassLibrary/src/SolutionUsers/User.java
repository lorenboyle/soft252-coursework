/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SolutionUsers;

import java.io.Serializable;
import static java.lang.Character.toLowerCase;


/**
 *
 * @author Loren Boyle
 */
public abstract class User implements Serializable {
    protected String username;
    protected String password;
    protected int age;
    protected char gender;
    protected boolean active;
    protected boolean terminate;
    
    public User(){
    }
    
    
    public User(String username, String password, int age, char gender, boolean active, boolean terminate) {
        this.username = username;
        this.password = password;
        this.age = age;
        this.gender = gender;        
        this.active = active;
        this.terminate = terminate;
    }

    public String getUsername() {
        return this.username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getPassword(){
        return password;
    }

    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public int getAge() {
        return age;
    }
    
    public void setAge(int age){
        if(age > 0) {
            this.age = age;
        } else {
            age = 0;
        }     
    }
    
    public char getGender() {
        return gender;
    }
    
    public void setGender(char gender) {
        gender = toLowerCase(gender);
        
        if(gender == 'm' || gender == 'f') {
            this.gender = gender;
        }
    }
    
    public boolean getActive() {
        return active;
    }
    
    public void setActive(boolean currentActive) {
        this.active = currentActive;
    }
    
    public void setTerminate(boolean terminate) {
        this.terminate = terminate;
    }
    
    
    public boolean getTerminate() {
        return terminate;
    } 
   
}
