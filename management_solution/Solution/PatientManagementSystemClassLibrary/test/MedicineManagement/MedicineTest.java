/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MedicineManagement;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Loren Boyle
 */
public class MedicineTest {
    
    public MedicineTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getQuantity method, of class Medicine.
     */
    @Test
    public void testGetQuantity() {
        System.out.println("getQuantity");
        Medicine instance = null;
        int expResult = 0;
        int result = instance.getQuantity();
        assertEquals(expResult, result);

    }

    /**
     * Test of setQuantityDecrease method, of class Medicine.
     */
    @Test
    public void testSetQuantityDecrease() {
        System.out.println("setQuantityDecrease");
        int valueDecreasing = 0;
        Medicine instance = null;
        int expResult = 0;
        int result = instance.setQuantityDecrease(valueDecreasing);
        assertEquals(expResult, result);

    }

    /**
     * Test of setQuantityIncrease method, of class Medicine.
     */
    @Test
    public void testSetQuantityIncrease() {
        System.out.println("setQuantityIncrease");
        int valueIncrease = 1;
        Medicine instance = new Medicine("testMed", "testDosage", 0);
        int expResult = 1;
        int result = instance.setQuantityIncrease(valueIncrease);
        assertEquals(expResult, result);

    }

    /**
     * Test of getMedName method, of class Medicine.
     */
    @Test
    public void testGetMedName() {
        System.out.println("getMedName");
        Medicine instance = null;
        String expResult = "";
        String result = instance.getMedName();
        assertEquals(expResult, result);

    }

    /**
     * Test of getMedDosage method, of class Medicine.
     */
    @Test
    public void testGetMedDosage() {
        System.out.println("getMedDosage");
        Medicine instance = null;
        String expResult = "";
        String result = instance.getMedDosage();
        assertEquals(expResult, result);

    }

    /**
     * Test of getRequestOrder method, of class Medicine.
     */
    @Test
    public void testGetRequestOrder() {
        System.out.println("getRequestOrder");
        Medicine instance = new Medicine.NewMedsBuilder("medName", "dosage").requestSecOrder().setQuantity().createMed(); 
        boolean expResult = true;
        boolean result = instance.getRequestOrder();
        assertEquals(expResult, result);

    }
    
}
