/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SolutionUsers;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Loren Boyle
 */
public class UserTest {
    
    public UserTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getUsername method, of class User.
     */
    @Test
    public void testGetUsername() {
        System.out.println("getUsername");
        User instance = new Patient("username", "password", 0, 'm', true, false);
        String expResult = "username";
        String result = instance.getUsername();
        assertEquals(expResult, result);
    }

    /**
     * Test of setUsername method, of class User.
     */
    @Test
    public void testSetUsername() {
        System.out.println("setUsername");
        String username = "";
        User instance = new UserImpl();
        instance.setUsername(username);

    }

    /**
     * Test of getPassword method, of class User.
     */
    @Test
    public void testGetPassword() {
        System.out.println("getPassword");
        User instance = new Admin("username", "password", 0, 'm', true, false);
        String expResult = "password";
        String result = instance.getPassword();
        assertEquals(expResult, result);

    }

    /**
     * Test of setPassword method, of class User.
     */
    @Test
    public void testSetPassword() {
        System.out.println("setPassword");
        String password = "";
        User instance = new UserImpl();
        instance.setPassword(password);

    }

    /**
     * Test of getAge method, of class User.
     */
    @Test
    public void testGetAge() {
        System.out.println("getAge");
        User instance = new UserImpl();
        int expResult = 0;
        int result = instance.getAge();
        assertEquals(expResult, result);
    }

    /**
     * Test of setAge method, of class User.
     */
    @Test
    public void testSetAge() {
        System.out.println("setAge");
        int age = 0;
        User instance = new UserImpl();
        instance.setAge(age);

    }

    /**
     * Test of getGender method, of class User.
     */
    @Test
    public void testGetGender() {
        System.out.println("getGender");
        User instance = new Patient("username", "password", 0, 'm', true, false);
        char expResult = 'm';
        char result = instance.getGender();
        assertEquals(expResult, result);

    }

    /**
     * Test of setGender method, of class User.
     */
    @Test
    public void testSetGender() {
        System.out.println("setGender");
        char gender = ' ';
        User instance = new UserImpl();
        instance.setGender(gender);

    }

    /**
     * Test of getActive method, of class User.
     */
    @Test
    public void testGetActive() {
        System.out.println("getActive");
        User instance = new UserImpl();
        boolean expResult = false;
        boolean result = instance.getActive();
        assertEquals(expResult, result);

    }

    /**
     * Test of setActive method, of class User.
     */
    @Test
    public void testSetActive() {
        System.out.println("setActive");
        boolean currentActive = false;
        User instance = new UserImpl();
        instance.setActive(currentActive);
    }

    /**
     * Test of setTerminate method, of class User.
     */
    @Test
    public void testSetTerminate() {
        System.out.println("setTerminate");
        boolean terminate = false;
        User instance = new UserImpl();
        instance.setTerminate(terminate);

    }

    /**
     * Test of getTerminate method, of class User.
     */
    @Test
    public void testGetTerminate() {
        System.out.println("getTerminate");
        User instance = new UserImpl();
        boolean expResult = false;
        boolean result = instance.getTerminate();
        assertEquals(expResult, result);

    }

    public class UserImpl extends User {
    }
    
}
